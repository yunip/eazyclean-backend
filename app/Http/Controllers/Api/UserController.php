<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends ApiController
{
    public function index() {
        $user = Auth::guard('api')->user();

        $data = Customer::find($user->customer_id);
        if(!$data){
            return $this->setStatusCode(401)->makeResponse(null, 'Failed To Retrieve Data', [], 'error');
        }

        return $this->setStatusCode(200)->makeResponse($data, 'Success Retrieve User');
    }
}
